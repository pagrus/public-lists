# public lists

This aims to be a few lists of things I like, or find useful, or
whatever.

Not sure yet what the format will be. Maybe JSON or SQL, or
possibly even both? I will probably keep the lists themselves in
a sqlite db and script something to dump them out and push.

I don't really have a specific plan, and maybe someone else has
already done this better. We'll see I guess

## vegan-sf
A list of vegan restaurants, vegetarian restaurants that have
vegan things, and vegan dishes at otherwise non-vegetarian
places.

key:
- v restaurant that has vegan items i like
- vv vegetarian restaurant with vegan options
- vvv all-vegan restaurant

## coffee shop
A list of coffee shops I like, maybe with notes

## in-n-out locations
Location data for In-N-Out Burger shops including counties
