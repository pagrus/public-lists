# If I were in charge of deciding which games shipped with the PlayStation
# Classic, this is what I would choose

- Castlevania SOTN
- Final Fantasy Tactics
- Tekken 2
- Bushido Blade
- Dynasty Warriors
- Soul Blade
- FFVII
- Tomb Raider
- MGS
- Gran Turismo
- Resident Evil
- Ridge Racer Revolution

